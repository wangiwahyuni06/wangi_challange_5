# Car Management Dashboard
description ...
    

## How to run

mengaktifkan yarn 
    └── yarn install

install sequelize ..
   └── npx sequelize init
   └── (buka folder config.json)
        ganti username, password, database, dan dialect

membuat database..
   └── npx sequelize db:create
   └── npx sequelize model:generate --name cars  --attributes name:string,price:integer,size:string,photo:text
   └── npx sequelize db:migrate

mengaktifkan server...
   └──yarn start


## Endpoints
list endpoints ...

## Directory Structure

```
.
├── config
│   └──config.json
├── controllers
│   └── carsController.js
├── migrations
│   └── 20220422034654-create-cars.js
├── models
│   └── cars.js
│   └── index.js
├── public
│   ├── css
│   ├── imges
│   └── js
├── seeders
├── services
│   └── carsService.js
├── views
│   └── create.ejs
│   └── edit.ejs
│   └── index.ejs
├── .gitignore
├── README.md
├── index.js
├── package-lock.json
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/images/erd.png)

