const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

// PORT with the default 3000
const PORT = process.env.PORT || 3000;

// Path in PUBLIC Directory
app.use(express.static(path.join(__dirname, "public")));
// Set View Engine
app.set("view engine", "ejs");

// import controller
const carsController = require("./controllers/carsController");
const req = require("express/lib/request");
const res = require("express/lib/response");

// endpoint crud
app.post("/cars", carsController.createCars);
// app.put("/cars", carsController.updateCars);

// define endpoint ejs
app.get("/", async (req, res) => {
  const getCars = await carsController.getCars();
  res.render("index", {
    cars: getCars,
  });
});

app.get("/cars", (req, res) => {
  res.render("create");
});

app.get("/edit", (req, res) => {
  res.render("edit");
})

// Listening to PORT
app.listen(PORT, () => {
  console.log(`Server nyala di http://localhost:${PORT}`);
});
